import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/subscribe to r2devops/i);
  expect(linkElement).toBeInTheDocument();
});
